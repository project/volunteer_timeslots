README for volunteer timeslots module for Drupal 5.x:

Installation:
----------

The event module *must* be installed and configured before you install this 
module.  This module depends upon the event module to function.

Upload this module's folder with all files to your modules directory and 
activate at admin/modules. The new table will be created automatically and
options set using the install file.

This module can also use information in user profiles created using the profile
module.  However, the profile module is not required and may be activated or 
configured after this module is installed.

Using the module:
----------

This module defines a new node type "event with volunteer timeslots".  Assign
permissions to create and edit this content type at admin/access.  Users with
the "administer volunteer events" permission will be able to edit all events
created with this module, regardless of who the original author is.  In 
addition, users with this permission can clear all volunteers from the event
via a check box on the node edit form.  Finally, users with this permission
as well as the node's author and the event's organizers can remove or add 
individual volunteers when viewing the event's time slots. The event's 
organizers may also all edit the event.

Change the settings at admin/settings/volunteer_timeslots to prevent the
node author from adding or removing volunteers, or change the setting to
prevent organizers from being able to edit the event.  You can also change
the timing of reminder e-mails (see below). Finally, if the profile module
is enabled you can choose from among the public and private (but not hidden)
profile fields to disaply along with the user name in the volunteer roster for 
each event.  This might be used, for example, to dsiplay the real name and phone
number of each user who volunteered if you wish to contact the volunteers 
before the event.
 
When creating a new event, be sure to set both the start and end times.  You 
will also need to supply at least one local username as an organizer, choose
the duration of each time slot, and the maximum number of volunteers for each 
slot.  The last time slot may be shorter than the others if the time between the
event start and end is not an integer multiple of the selected duration per slot.
This module enforces a minimum event duration equal to the duration of one slot.

All registered users of your site will be able to sign up for one or more
time slots for each event.  After checking/unchecking the box in that
time slot, they submit the form. A new revision of the node is saved with 
the updated volunteer information, and the user will receive an e-mail with
the event's information if the checkbox at the bottom of the form is set.
The e-mail address of the first organizer is used as the return address.

This module implements a cron hook to send a reminder e-mail in advance
to all users who have volunteered for an event.  The timing of the e-mail 
depends upon the value set in the module settings, as well as when and how often
cron.php is run on your site. The e-mail address of the first organizer is
used as the return address.  Only one reminder is sent- users who sign up
after this point will not get a reminder. 

Theming:
-------

The file volunteer_timeslots.css contains CSS that is added to every page.
This will be more efficient if you turn on the CSS aggregator at 
admin/settings/performance

This module only defines two themable functions: 

theme_timeslots_signup_form  
This function organizes the form elements of the signup form so that they are
displayed in a table, and adds the class "volunteer-timeslots" to the table.

theme_timeslots_roster_form
This function organized the volunteer roster and the texfields for notes that
are visible to admins below the signup form.

Trouble shooting:
--------
Some users have experienced an error where only one time slot is shown or 
created in the event form.  The reason for this is that this modules
requeries some processing of the date fields by the event module to
occur before it can understand/manipulate them.

If you experience this problem, check the {system} table and make sure
that this module's weight is set to be equal to or greater than that of 
the event module.  If you are not familiar with the necessary SQL, you
can try using the module weight module: 
http://drupal.org/project/moduleweight
